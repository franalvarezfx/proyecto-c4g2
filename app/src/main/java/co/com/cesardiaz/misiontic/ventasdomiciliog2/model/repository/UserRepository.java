package co.com.cesardiaz.misiontic.ventasdomiciliog2.model.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.database.SalesDatabase;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.database.dao.UserDao;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.database.entity.User;

public class UserRepository {

    private final UserDao userDao;
    private final DatabaseReference userRef;

    private final Boolean inDB = false;

    public UserRepository(Context context) {
        this.userDao = SalesDatabase.getInstance(context)
                .getUserDao();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef = database.getReference("users");

        //loadInitialUsers();
    }

    private void loadInitialUsers() {
        if (inDB) {
            // Usar Room Database
            userDao.insert(new User("Cesar Diaz", "cdiaz@email.com", "12345678"));
            userDao.insert(new User("User Test", "test@email.com", "87654321"));
        } else {
            userRef.child("id").setValue("3");
            userRef.child("next").removeValue();
            // Usar Firebase
            userRef.child("cdiaz_email_com").child("name").setValue("Cesar Diaz");
            userRef.child("cdiaz_email_com").child("email").setValue("cdiaz@email.com");
            userRef.child("cdiaz_email_com").child("password").setValue("12345678");

            User user = new User("User Test", "test@email.com", "87654321");
            userRef.child(getEmailId(user.getEmail())).setValue(user);
        }
    }

    public void getAllUsers(UserCallback<List<User>> callback){
        if(inDB){
            callback.onSuccess(userDao.getAll());
        } else {
            userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    userRef.removeEventListener(this);

                    List<User> users = new ArrayList<>();
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        users.add(snapshot.getValue(User.class));
                    }
                    callback.onSuccess(users);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    userRef.removeEventListener(this);

                    callback.onFailure();
                }
            });
        }
    }

    public void getUserByEmail(String email, UserCallback<User> callback) {
        if (inDB) {
            callback.onSuccess(userDao.getUserByEmail(email));
        } else {
            userRef.child(getEmailId(email)).get()
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()) {
                            User user = task.getResult().getValue(User.class);
                            callback.onSuccess(user);
                        } else {
                            callback.onFailure();
                        }
                    });
        }
    }

    public void createUser(User user, UserCallback<Void> callback){
        if(inDB){
            callback.onSuccess(null);
        } else {
            User userToSave = new User(user.getName(), user.getEmail(), null);
            userToSave.setEnable(userToSave.getEnable());

            userRef.child(user.getUid()).setValue(userToSave);

            callback.onSuccess(null);
        }
    }

    private String getEmailId(String email) {
        return email.replace('@', '_').replace('.', '_');
    }

    public static interface UserCallback<T> {
        void onSuccess(T node);

        void onFailure();
    }
}
