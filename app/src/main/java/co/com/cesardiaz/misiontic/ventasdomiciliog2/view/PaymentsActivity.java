package co.com.cesardiaz.misiontic.ventasdomiciliog2.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseUser;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.R;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.repository.FirebaseAuthRepository;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.mvp.PaymentsMVP;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.presenter.PaymentsPresenter;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.view.adapters.PaymentsAdapter;

public class PaymentsActivity extends AppCompatActivity implements PaymentsMVP.View {

    private LinearProgressIndicator pbWait;

    private DrawerLayout drawerLayout;
    private MaterialToolbar toolbar;
    private NavigationView navigationView;

    private TextView tvEmail;

    private RecyclerView rvPayments;
    private FloatingActionButton btnNewSale;

    private PaymentsMVP.Presenter presenter;
    private PaymentsAdapter paymentsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        presenter = new PaymentsPresenter(PaymentsActivity.this);

        initUI();

        loadData();
    }

    private void loadData() {
        presenter.loadPayments();

        FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance(this);
        FirebaseUser user = repository.getCurrentUser();
        tvEmail.setText(user.getEmail());
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.app_name)
                .setMessage("Esta seguro en cerrar la sesión?")
                .setPositiveButton("Si",
                        (dialog, which) -> {
                            FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance(PaymentsActivity.this);
                            repository.logOut();

                            PaymentsActivity.super.onBackPressed();
                        })
                .setNegativeButton("No", null);

        builder.create().show();
    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        toolbar = findViewById(R.id.app_toolbar);
        toolbar.setNavigationOnClickListener(v -> drawerLayout.openDrawer(navigationView));

        pbWait = findViewById(R.id.pb_wait);

        navigationView = findViewById(R.id.nv_payments);
        navigationView.setNavigationItemSelectedListener(this::onMenuItemClick);

        rvPayments = findViewById(R.id.rv_payments);
        rvPayments.setLayoutManager(new LinearLayoutManager(PaymentsActivity.this));
        paymentsAdapter = new PaymentsAdapter();
        paymentsAdapter.setListener(item -> presenter.onSelectItem(item));
        rvPayments.setAdapter(paymentsAdapter);


        btnNewSale = findViewById(R.id.btn_new_sale);
        btnNewSale.setOnClickListener(v -> onNewSaleClick());

        tvEmail = navigationView.getHeaderView(0).findViewById(R.id.tv_email);
    }

    private boolean onMenuItemClick(MenuItem menuItem) {
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();
        return true;
    }

    private void onNewSaleClick() {
        startActivity(new Intent(PaymentsActivity.this, NewSaleActivity.class));
    }

    @Override
    public Activity getActivity() {
        return PaymentsActivity.this;
    }

    @Override
    public void showProgressBar() {
        pbWait.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        pbWait.setVisibility(View.GONE);
    }

    @Override
    public void showPayments(List<PaymentsMVP.PaymentDto> payments) {
        paymentsAdapter.setData(payments);
    }

    @Override
    public void openDetailsActivity(Bundle params) {
        Intent intent = new Intent(PaymentsActivity.this, NewSaleActivity.class);
        intent.putExtras(params);
        //startActivity(intent);
        Toast.makeText(PaymentsActivity.this, params.getString("name"), Toast.LENGTH_SHORT)
                .show();
    }

}