package co.com.cesardiaz.misiontic.ventasdomiciliog2.mvp;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

public interface PaymentsMVP {

    interface Model {
        void loadPayments(LoadPaymentsCallback callback);

        interface LoadPaymentsCallback {
            void setPayments(List<PaymentDto> payments);
        }
    }

    interface Presenter {
        void loadPayments();

        void onPaymentsClick();

        void onNewSaleClick();

        void onSelectItem(PaymentDto item);
    }

    interface View {
        Activity getActivity();

        void showProgressBar();

        void hideProgressBar();

        void showPayments(List<PaymentDto> payments);

        void openDetailsActivity(Bundle params);
    }

    class PaymentDto {
        private String client;
        private String address;

        public PaymentDto(String client, String address) {
            this.client = client;
            this.address = address;
        }

        public String getClient() {
            return client;
        }

        public String getAddress() {
            return address;
        }
    }
}
