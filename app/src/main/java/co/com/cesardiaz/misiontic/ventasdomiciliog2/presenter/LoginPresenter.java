package co.com.cesardiaz.misiontic.ventasdomiciliog2.presenter;

import android.content.Intent;
import android.util.Log;

import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog2.model.LoginInteractor;
import co.com.cesardiaz.misiontic.ventasdomiciliog2.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

    private final LoginMVP.View view;
    private final LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view) {
        this.view = view;
        this.model = new LoginInteractor(view.getActivity());
    }

    @Override
    public void onLoginClick() {
        boolean error = false;
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();

        //Valido datos
        view.showEmailError("");
        view.showPasswordError("");
        if (loginInfo.getEmail().isEmpty()) {
            view.showEmailError("Correo electrónico es obligatorio");
            error = true;
        } else if (!isEmailValid(loginInfo.getEmail())) {
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }

        if (loginInfo.getPassword().isEmpty()) {
            view.showPasswordError("Contraseña es obligatoria");
            error = true;
        } else if (!isPasswordValid(loginInfo.getPassword())) {
            view.showPasswordError("Contraseña no cumple criterios de seguridad");
            error = true;
        }

        if (!error) {
            view.showProgresBar();
            new Thread(() ->
                    model.validateCredentials(loginInfo.getEmail(), loginInfo.getPassword(),
                    new LoginMVP.Model.ValidateCredentialsCallback() {
                        @Override
                        public void onSuccess() {
                            view.getActivity().runOnUiThread(() -> {
                                view.showPaymentsActivity();
                                view.hideProgresBar();
                            });
                        }

                        @Override
                        public void onFailure() {
                            view.getActivity().runOnUiThread(() -> {
                                view.hideProgresBar();
                                view.showGeneralError("Credenciales inválidas");
                            });
                        }
                    })).start();

            new Thread(() ->
                    model.getAllUsers(new LoginMVP.Model.GetUserCallback<List<LoginMVP.UserInfo>>() {
                        @Override
                        public void onSuccess(List<LoginMVP.UserInfo> data) {
                            for (LoginMVP.UserInfo user : data) {
                                Log.i(LoginPresenter.class.getSimpleName(), user.toString());
                            }
                        }

                        @Override
                        public void onFailure() {
                            Log.e(LoginPresenter.class.getSimpleName(), "No hay datos en base de datos");
                        }
                    })).start();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@")
                && email.endsWith(".com");
    }

    @Override
    public void onFacebookClick() {

    }

    @Override
    public void onGoogleClick() {
        Intent intent = model.getGoogleIntent();
        view.showGoogleSignInActivity(intent);
    }

    @Override
    public void validateHasUserAuthenticated() {
        if (model.hasAuthenticatedUser()) {
            view.showPaymentsActivity();
        }
    }

    @Override
    public void setGoogleData(Intent data) {
        model.setGoogleData(data, new LoginMVP.Model.ValidateCredentialsCallback() {
            @Override
            public void onSuccess() {
                view.getActivity().runOnUiThread(() -> {
                    view.showPaymentsActivity();
                    view.hideProgresBar();
                });
            }

            @Override
            public void onFailure() {
                view.getActivity().runOnUiThread(() -> {
                    view.hideProgresBar();
                    view.showGeneralError("No pudo acceder a su cuenta de google");
                });
            }
        });
    }

}
